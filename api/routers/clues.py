# complete, yay!
# https://learn-2.galvanize.com/cohorts/3189/blocks/1893/content_files/build/04-unstructured-data/65-trivia-game-clues.md

from fastapi import APIRouter, Response, status, HTTPException
from pydantic import BaseModel
import psycopg
from .categories import CategoryOut

router = APIRouter()


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


# class Clues(BaseModel):
#     page_count: int
#     clues: list[ClueOut]


class Clues(BaseModel):
    clues: list[ClueOut]


class Message(BaseModel):
    message: str


@router.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    # responses={404: {"model": Message}},
)
# def get_clue(clue_id: int, response: Response):
def get_clue(clue_id: int):

    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clu.id, clu.answer, clu.question, clu.value, clu.invalid_count,
                    cat.id, cat.title, cat.canon, clu.canon
                FROM clues AS clu
                INNER JOIN categories AS cat
                ON (cat.id = clu.category_id)
                WHERE clu.id = %s
            """,
                [clue_id],
            )
            row = cur.fetchone()
            if row is None:
                raise HTTPException(status_code=404, detail="Clue not found")
                # Below 2 lines work when there isn't a response_model=ClueOut (specific class) that restricts the return output to be in the format of the ClueOut class.
                # But because there is a response_model=ClueOut, when a request with clue_id=10000000 (non-existent in db) is sent, then
                # the SQL query returns nothing, so the return of this function does NOT match the expected format of a ClueOut class.
                # so intead of returning the message of "Clue not found", it throws a 500 internal server error.
                # In order to keep BOTH the "Clue not found" message and the ClueOut class as the response_model, we can use the above 1 line.
                # By raising an HTTPException, it just raises the error, returns {detail: "Clue not found"} to user, and doesn't even run the lines below.
                # response.status_code = status.HTTP_404_NOT_FOUND
                # return {"message": "Clue not found"}
            clue = {
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category": {
                    "id": row[5],
                    "title": row[6],
                    "canon": row[7],
                },
                "canon": row[8],
            }

            return clue


@router.get(
    "/api/clues/random-clue/",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_random_clue(response: Response, valid: bool = True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if valid:
                cur.execute(
                    """
                    SELECT clu.id, clu.answer, clu.question, clu.value, clu.invalid_count,
                        cat.id, cat.title, cat.canon, clu.canon
                    FROM clues AS clu
                    INNER JOIN categories AS cat
                    ON (cat.id = clu.category_id)
                    WHERE invalid_count = 0
                    ORDER BY RANDOM() LIMIT 1
                """,
                )
                row = cur.fetchone()
            else:
                cur.execute(
                    """
                    SELECT clu.id, clu.answer, clu.question, clu.value, clu.invalid_count,
                        cat.id, cat.title, cat.canon, clu.canon
                    FROM clues AS clu
                    INNER JOIN categories AS cat
                    ON (cat.id = clu.category_id)
                    ORDER BY RANDOM() LIMIT 1
                """,
                )
                row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            clue = {
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category": {
                    "id": row[5],
                    "title": row[6],
                    "canon": row[7],
                },
                "canon": row[8],
            }

            return clue


@router.get("/api/clues", response_model=Clues)
def clues_list(page: int = 0, value: int = -1):
    # Uses the environment variables to connect
    # In development, see the docker-compose.yml file for
    #   the PG settings in the "environment" section
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if value >= 0:
                # Need c.invalid_count = 0 because some clues have invalid_count = null, and otherwise, it'll raise an error with the response_model
                cur.execute(
                    """
                    SELECT c.id, c.answer, c.question, c.value, c.invalid_count,
                        cat.id, cat.title, cat.canon, c.canon
                    FROM clues AS c
                    INNER JOIN categories AS cat
                    ON (cat.id = c.category_id)
                    WHERE c.invalid_count = 0
                    AND c.value = %s
                    ORDER BY title
                    LIMIT 100 OFFSET %s
                """,
                    [value, page * 100],
                )

                rows = cur.fetchall()

            else:
                # Need c.invalid_count = 0 because some clues have invalid_count = null, and otherwise, it'll raise an error with the response_model
                cur.execute(
                    """
                    SELECT c.id, c.answer, c.question, c.value, c.invalid_count,
                        cat.id, cat.title, cat.canon, c.canon
                    FROM clues AS c
                    INNER JOIN categories AS cat
                    ON (cat.id = c.category_id)
                    WHERE c.invalid_count = 0
                    ORDER BY title
                    LIMIT 100 OFFSET %s
                """,
                    [page * 100],
                )

                rows = cur.fetchall()

            formatted = []
            for row in rows:
                clue = {
                    "id": row[0],
                    "answer": row[1],
                    "question": row[2],
                    "value": row[3],
                    "invalid_count": row[4],
                    "category": {
                        "id": row[5],
                        "title": row[6],
                        "canon": row[7],
                    },
                    "canon": row[8],
                }
                formatted.append(clue)
            return Clues(clues=formatted)


@router.delete(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def delete_but_really_update_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE id = %s;
            """,
                [clue_id],
            )
            return get_clue(clue_id, response)
