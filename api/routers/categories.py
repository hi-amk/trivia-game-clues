from fastapi import APIRouter, Response, status, HTTPException
from pydantic import BaseModel
import psycopg
import pymongo

# import bson
import os

dbhost = os.environ["MONGOHOST"]
dbname = os.environ["MONGODATABASE"]
dbuser = os.environ["MONGOUSER"]
dbpass = os.environ["MONGOPASSWORD"]
mongo_str = f"mongodb://{dbuser}:{dbpass}@{dbhost}"

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class CategoryIn(BaseModel):
    title: str


class CategoryOut(BaseModel):
    id: int
    title: str
    canon: bool


class CategoryWithClueCount(CategoryOut):
    num_clues: int


class Categories(BaseModel):
    page_count: int
    categories: list[CategoryWithClueCount]


class Message(BaseModel):
    message: str


# ORIGINAL categories_list function by querying the POSTGRESQL database
# SEE BELOW for categories_list function by querying the MONGODB database
# @router.get("/api/categories", response_model=Categories)
# def categories_list(page: int = 0):
#     # Uses the environment variables to connect
#     # In development, see the docker-compose.yml file for
#     #   the PG settings in the "environment" section
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 SELECT categories.id, categories.title, categories.canon, count(*) AS num_clues
#                 FROM categories
#                 LEFT OUTER JOIN clues
#                 ON (clues.category_id = categories.id)
#                 GROUP BY categories.id, categories,title, categories.canon
#                 ORDER BY categories.title
#                 LIMIT 100 OFFSET %s
#             """,
#                 [page * 100],
#             )

#             results = []
#             for row in cur.fetchall():
#                 record = {}
#                 for i, column in enumerate(cur.description):
#                     record[column.name] = row[i]
#                 results.append(record)

#             cur.execute(
#                 """
#                 SELECT COUNT(*) FROM categories;
#             """
#             )
#             raw_count = cur.fetchone()[0]
#             page_count = (raw_count // 100) + 1

#             return Categories(page_count=page_count, categories=results)


# ORIGINAL categories_list function by querying the POSTGRESQL database IS ABOVE
# SEE BELOW for categories_list function by querying the MONGODB database
@router.get("/api/categories", response_model=Categories)
def categories_list(page: int = 0):
    # Creates a new MongoClient to talk to the DDBMS
    client = pymongo.MongoClient(mongo_str)
    # Gets the database from the value of an environment variable
    db = client[dbname]
    # Finds categories sorted by title, skipping the categories not needed due to the page parameter, and limiting the results to 100
    categories = db.categories.find().sort("title").skip(100 * page).limit(100)
    # Turn (typecast) the object returned from the query into a list
    categories = list(categories)
    # Loop over each category in the list:
    for category in categories:
        # Counts the number of clues with the specified category_id. In other words:
        # This line queries the clues collection  by "category_id", where "category_id" is the same as the _id field inside the categories collection.
        # Because we query the clues collection by "category_id" field and it takes a long time (~30 seconds in Curtis' demo) to make a GET request to list categories,
        # we create an index.
        # See bottom of page for instructions on how to create an index: https://learn-2.galvanize.com/cohorts/3189/blocks/1893/content_files/build/04-unstructured-data/66-trivia-game-refactoring.md
        # After creating the index (on the category_id field of documents in the clues collection), if we send a GET request to list categories, it takes less than 1 second.
        count = db.command(
            {"count": "clues", "query": {"category_id": category["_id"]}}
        )
        # Adds the count of clues to the category
        category["num_clues"] = count["n"]
        # Copies the value of _id to id in the dictionary
        category["id"] = category["_id"]
        # Delete the _id value
        del category["_id"]
    # Gets the total count of category pages
    page_count = db.command({"count": "categories"})["n"] // 100
    # Returns the content
    return {
        "page_count": page_count,
        "categories": categories,
    }


# ORIGINAL get_category function by querying the POSTGRESQL database
# SEE BELOW for get_category function by querying the MONGODB database
# @router.get(
#     "/api/categories/{category_id}",
#     response_model=CategoryOut,
#     responses={404: {"model": Message}},
# )
# def get_category(category_id: int, response: Response):
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 SELECT id, title, canon
#                 FROM categories
#                 WHERE id = %s
#             """,
#                 [category_id],
#             )
#             row = cur.fetchone()
#             if row is None:
#                 raise HTTPException(status_code=404, detail="Category not found")
#                 # Below 2 lines work when there isn't a response_model=CategoryOut (specific class) that restricts the return output to be in the format of the CategoryOut class.
#                 # But because there is a response_model=CategoryOut, when a request with category_id=10000000 (non-existent in db) is sent, then
#                 # the SQL query returns nothing, so the return of this function does NOT match the expected format of a CategoryOut class.
#                 # so intead of returning the message of "Category not found", it throws a 500 internal server error.
#                 # In order to keep BOTH the "Category not found" message and the CategoryOut class as the response_model, we can use the above 1 line.
#                 # By raising an HTTPException, it just raises the error, returns {detail: "Category not found"} to user, and doesn't even run the lines below.
#                 # response.status_code = status.HTTP_404_NOT_FOUND
#                 # return {"message": "Category not found"}
#             record = {}
#             for i, column in enumerate(cur.description):
#                 record[column.name] = row[i]
#             return record


# ORIGINAL get_category function by querying the POSTGRESQL database IS ABOVE
# SEE BELOW for get_category function by querying the MONGODB database
# https://learn-2.galvanize.com/cohorts/3189/blocks/1893/content_files/build/04-unstructured-data/66-trivia-game-refactoring.md
@router.get(
    "/api/categories/{category_id}",
    response_model=CategoryOut,
    responses={404: {"model": Message}},
)
def get_category(category_id: int, response: Response):
    # Creates a new MongoClient to talk to the DDBMS
    client = pymongo.MongoClient(mongo_str)
    # Gets the database from the value of an environment variable
    db = client[dbname]
    print("THIS IS THE PRINT CONFIRMING WE'RE CONNECTED TO THE MONGO DATABASE", db)
    # Finds one document based on the id
    category = db.categories.find_one({"_id": category_id})
    # print(category)
    # Copies the value of _id to id in the dictionary
    category["id"] = category["_id"]
    # Delete the _id value
    del category["_id"]
    # Returns the dictionary that represents the category
    return category


@router.post(
    "/api/categories",
    response_model=CategoryOut,
    responses={409: {"model": Message}},
)
def create_category(category: CategoryIn, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                # Uses the RETURNING clause to get the data
                # just inserted into the database. See
                # https://www.postgresql.org/docs/current/sql-insert.html
                cur.execute(
                    """
                    INSERT INTO categories (title, canon)
                    VALUES (%s, false)
                    RETURNING id, title, canon;
                """,
                    [category.title],
                )
            except psycopg.errors.UniqueViolation:
                # status values at https://github.com/encode/starlette/blob/master/starlette/status.py
                response.status_code = status.HTTP_409_CONFLICT
                return {
                    "message": "Could not create duplicate category",
                }
            row = cur.fetchone()
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record


@router.put(
    "/api/categories/{category_id}",
    response_model=CategoryOut,
    responses={404: {"model": Message}},
)
def update_category(category_id: int, category: CategoryIn, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE categories
                SET title = %s
                WHERE id = %s;
            """,
                [category.title, category_id],
            )
    return get_category(category_id, response)


@router.delete(
    "/api/categories/{category_id}",
    response_model=Message,
    responses={400: {"model": Message}},
)
def remove_category(category_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(
                    """
                    DELETE FROM categories
                    WHERE id = %s;
                """,
                    [category_id],
                )
                return {
                    "message": "Success",
                }
            except psycopg.errors.ForeignKeyViolation:
                response.status_code = status.HTTP_400_BAD_REQUEST
                return {
                    "message": "Cannot delete category because it has clues",
                }
