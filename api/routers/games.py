from fastapi import APIRouter, Response, status, HTTPException
from pydantic import BaseModel
from datetime import datetime
import psycopg

router = APIRouter()


class GameOut(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_amount_won: int


class Message(BaseModel):
    message: str


@router.get(
    "/api/games/{game_id}",
    response_model=GameOut,
    # responses={404: {"model": Message}},
)
# def get_game(game_id: int, response: Response):
def get_game(game_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT games.id, games.episode_id, games.aired, games.canon, SUM(clues.value)
                FROM games
                LEFT OUTER JOIN clues
                ON (clues.game_id = games.id)
                WHERE games.id = %s
                GROUP BY games.id, games.episode_id, games.aired, games.canon
            """,
                [game_id],
            )
            row = cur.fetchone()
            if row is None:
                raise HTTPException(status_code=404, detail="Game not found")
                # Below 2 lines work when there isn't a response_model=GameOut (specific class) that restricts the return output to be in the format of the GameOut class.
                # But because there is a response_model=GameOut, when a request with game_id=10000000 (non-existent in db) is sent, then
                # the SQL query returns nothing, so the return of this function does NOT match the expected format of a GameOut class.
                # so intead of returning the message of "Game not found", it throws a 500 internal server error.
                # In order to keep BOTH the "Game not found" message and the GameOut class as the response_model, we can use the above 1 line.
                # By raising an HTTPException, it just raises the error, returns {detail: "Game not found"} to user, and doesn't even run the lines below.
                # response.status_code = status.HTTP_404_NOT_FOUND
                # return {"message": "Game not found"}
            record = {
                "id": row[0],
                "episode_id": row[1],
                "aired": row[2],
                "canon": row[3],
                "total_amount_won": row[4],
            }

            return record


class Category(BaseModel):
    id: int
    title: str


class Clues(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: Category


class CustomGame(BaseModel):
    id: int
    created_on: datetime
    clues: list[Clues]


@router.post(
    "/api/custom-games",
    response_model=CustomGame,
    responses={404: {"model": Message}},
)
def create_custom_game():
    with psycopg.connect(autocommit=True) as conn:

        cur = conn.cursor()

        cur.execute(
            """
            SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count,
                categories.id, categories.title
            FROM clues
            INNER JOIN categories
            ON (categories.id = clues.category_id)
            WHERE clues.canon = true
            ORDER BY RANDOM() LIMIT 30
        """
        )
        thirty_random_clues = cur.fetchall()

        # https://www.psycopg.org/psycopg3/docs/basic/transactions.html#transaction-context
        # The connection is autocommit, so no BEGIN executed.
        with conn.transaction():
            # # BEGIN is executed, a transaction started
            cur.execute(
                """
                INSERT INTO game_definitions (created_on)
                VALUES (CURRENT_TIMESTAMP)
                RETURNING id, created_on;
            """
            )

            row = cur.fetchone()
            new_game_id = row[0]
            new_game_created_on = row[1]

            for clue in thirty_random_clues:
                clue_id = clue[0]

                cur.execute(
                    """
                    INSERT INTO game_definition_clues (game_definition_id, clue_id)
                    VALUES (%s, %s);
                """,
                    [new_game_id, clue_id],
                )

        # These two operation run atomically in the same transaction

        # COMMIT is executed at the end of the block.
        # The connection is in idle state again.

        # The connection is closed at the end of the block.

        custom_game = {
            "id": new_game_id,
            "created_on": new_game_created_on,
            "clues": [],
        }

        for clue in thirty_random_clues:
            formatted_clue = {
                "id": clue[0],
                "answer": clue[1],
                "question": clue[2],
                "value": clue[3],
                "invalid_count": clue[4],
                "category": {"id": clue[5], "title": clue[6]},
            }
            custom_game["clues"].append(formatted_clue)

        return custom_game


@router.get(
    "/api/custom-games/{custom_game_id}",
    response_model=CustomGame,
    # responses={404: {"model": Message}},
)
def get_custom_game(custom_game_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            # Not super sure about the RIGHT/LEFT OUTER JOINS (whether RIGHT or LEFT), but this is just what I wrote from instinct... and it works!
            cur.execute(
                """
                SELECT game_definitions.id, game_definitions.created_on,
                    clues.id, clues.answer, clues.question, clues.value, clues.invalid_count,
                    categories.id, categories.title
                FROM game_definitions

                RIGHT OUTER JOIN game_definition_clues
                ON (game_definitions.id = game_definition_clues.game_definition_id)

                RIGHT OUTER JOIN clues
                ON (clues.id = game_definition_clues.clue_id)

                LEFT OUTER JOIN categories
                ON (categories.id = clues.category_id)

                WHERE game_definitions.id = %s
            """,
                [custom_game_id],
            )
            custom_game = cur.fetchall()
            # print("CUSTOM GAMEEEEEEEEEEEEEEEEEEEEEEEEEEEEE", custom_game)
            # if custom_game is an empty list
            if len(custom_game) == 0:
                raise HTTPException(status_code=404, detail="Custom game not found")
                # Below 2 lines work when there isn't a response_model=GameOut (specific class) that restricts the return output to be in the format of the GameOut class.
                # But because there is a response_model=GameOut, when a request with game_id=10000000 (non-existent in db) is sent, then
                # the SQL query returns nothing, so the return of this function does NOT match the expected format of a GameOut class.
                # so intead of returning the message of "Game not found", it throws a 500 internal server error.
                # In order to keep BOTH the "Game not found" message and the GameOut class as the response_model, we can use the above 1 line.
                # By raising an HTTPException, it just raises the error, returns {detail: "Game not found"} to user, and doesn't even run the lines below.
                # response.status_code = status.HTTP_404_NOT_FOUND
                # return {"message": "Game not found"}

            formatted_custom_game = {
                "id": custom_game[0][0],
                "created_on": custom_game[0][1],
                "clues": [],
            }

            for clue in custom_game:
                formatted_clue = {
                    "id": clue[2],
                    "answer": clue[3],
                    "question": clue[4],
                    "value": clue[5],
                    "invalid_count": clue[6],
                    "category": {"id": clue[7], "title": clue[8]},
                }
                formatted_custom_game["clues"].append(formatted_clue)

            return formatted_custom_game
